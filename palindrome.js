function palindrome(str) {
    str = str.replace(/[\W_]/g, '').toUpperCase();
    let lIndex = str.length - 1;
    for (let rIndex = 0; rIndex < lIndex; rIndex++) {
        if (str[rIndex] !== str[lIndex]) {
            return false;
        }
        lIndex--;
    }
    return true;
}