function validateUSPhoneNumber(string){

   if(string.match(/^1?\s*(\({1}\w{3}\){1}|\w{3})-?\s*\w{3}-?\s*\w{4}$/)){
       console.log('valid number:',string);
       return true;
   }else{
       console.log('invalid number:',string);
       return false;
   }
}

/*
//Test

validateUSPhoneNumber("555-555-5555")
validateUSPhoneNumber("(555)555-5555")
validateUSPhoneNumber("(555) 555-5555")
validateUSPhoneNumber("555 555 5555")
validateUSPhoneNumber("5555555555")
validateUSPhoneNumber("1 555 555 5555")
validateUSPhoneNumber("1 555-555-5555")
validateUSPhoneNumber("1 (555) 555-5555")
validateUSPhoneNumber("1(555)555-5555")
validateUSPhoneNumber("1 456 789 4444")

console.log('')
validateUSPhoneNumber("555-5555")
validateUSPhoneNumber("5555555")
validateUSPhoneNumber("1 555)555-5555")
validateUSPhoneNumber("123**&!!asdf#")
validateUSPhoneNumber("55555555")
validateUSPhoneNumber("(6054756961)")
validateUSPhoneNumber("2 (757) 622-7382")

*/
