function rot13(sentence) {
    let LetterMap = createLetterMap(sentence)
    let charCodeMap = {}
    let adjustedMap = {}
    let decrypted = "";
    for (var key in LetterMap) {

        if (key.match(/[\W\d_]/)) {
            console.log("non alpabetic")
            charCodeMap[key] = LetterMap[key]

        } else {
            console.log(key, key.charCodeAt(0))
            console.log('alphabetic')
            charCodeMap[key.charCodeAt(0)] = LetterMap[key]
        }
    }

    /* Move the charCodeMap keys 13 steps backwards.

    A -> 65
    Z -> 90
*/
    for (var key in charCodeMap) {

        if (parseInt(key)) {
            let temp = parseInt(key) - 13
            if (temp < 65) {
                temp = 90 - (65 - temp) + 1
            }
            adjustedMap[temp] = charCodeMap[key]
        } else {
            adjustedMap[key] = charCodeMap[key]
        }
    }

    //Reconstruct the sentence back
    for (let i = 0; i < sentence.length; i++) {

        for (var key in adjustedMap) {
            if (adjustedMap[key].includes(i) && parseInt(key)) {
                decrypted += String.fromCharCode(key)
            } else if (adjustedMap[key].includes(i)) {
                decrypted += key
            }
        }
    }


    console.log(charCodeMap)
    console.log(adjustedMap)
    console.log(decrypted)

    return decrypted

}


function createLetterMap(sentence) {
    let LetterMap = {}
    for (let i = 0; i < sentence.length; i++) {
        if (!LetterMap[sentence[i]]) {
            LetterMap[sentence[i]] = [i]
        } else {
            LetterMap[sentence[i]].push(i)
        }
    }
    console.log(LetterMap)
    return LetterMap;
}


rot13("SERR PBQR PNZC")
rot13("SERR CVMMN")
rot13("SERR YBIR?")
rot13("GUR DHVPX OEBJA SBK WHZCF BIRE GUR YNML QBT")
