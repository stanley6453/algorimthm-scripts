
//all the special/unique letters in the roman numerals
const map = {
    1: 'I',
    5: 'V',
    10: 'X',
    50: 'L',
    100: 'C',
    500: 'D',
    1000: 'M',
    5000: "V-bar",
    10000: "X-bar",
    50000: "L-bar",
    100000: "C-bar",
    500000: "D-bar",
    1000000: "M-bar"
}


/*Notice that the unique numbers in the roman numerals are of 
the form 1*10^n,5*10^n,1*10^n+1,5*10^n+1.
temp will help us when dealing with unit tens, hundred, thousands...
the 10 at index 2 in the temp array is used to help us deal with 
unit, tens, hundreds... of 9 which needs the next 1*10^n+1 in the
sequence to compute its value.

this array starts with unit
*/
let temp = [1, 5, 10]


/*
Contains the logic of how to form/compute roman numerals
*/
function createNewFormatObject(){
    return {
        1: map[temp[0]],
        2: map[temp[0]] + map[temp[0]],
        3: map[temp[0]] + map[temp[0]] + map[temp[0]],
        4: map[temp[0]] + map[temp[1]],
        5: map[temp[1]],
        6: map[temp[1]] + map[temp[0]],
        7: map[temp[1]] + map[temp[0]] + map[temp[0]],
        8: map[temp[1]] + map[temp[0]] + map[temp[0]] + map[temp[0]],
        9: map[temp[0]] + map[temp[2]],
    }
}

//main function
function convertToRoman(number) {

    let base10 = unitTens(number);
    // let _base10 = addZerosToUnitTens(base10)

    let romanNumeral = convertArrayToRomanNumeral(base10)

    // console.log(number)
    // console.log(base10)
    // console.log(_base10)
    // console.log(romanNumeral)

 temp = [1, 5, 10]

    return romanNumeral

}

//breaks the number down into its respective unit tens hundred thousands etc...
// and returns it as an array with the unit as the first index and so on.
function unitTens(number) {
    let base10 = []
    while (number / 10) {
        base10.push(number % 10)
        number = parseInt(number / 10)
    }
    return base10
}

// function addZerosToUnitTens(base10) {
//     let _base10 = []
//     for (let i = 0; i < base10.length; i++) {
//         _base10[i] = base10[i]
//         for (let j = 0; j < i; j++) {
//             _base10[i] += '0'
//         }
//         _base10[i] = parseInt(_base10[i])

//     }
//     return _base10
// }


//Converts the units tens and hundreads array to roman numeral
function convertArrayToRomanNumeral(base10) {
    let romanNumeral = [];
    for (let i = 0; i < base10.length; i++) {
        romanNumeral.push(createNewFormatObject()[base10[i]]);
        // console.log(temp);

        //multiply the array elements by 10 to go from unit to tens 
        //to hundreds to thousands and so forth after each level is calculated
        temp.forEach((value, index) => {
            temp[index] = value * 10;
        })
    }
    return romanNumeral.reverse().join("");
}



console.log(convertToRoman(6))
console.log(convertToRoman(6))
