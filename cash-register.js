const currencyUnit = {
    'ONE HUNDRED': 100,
    TWENTY: 20,
    TEN: 10,
    FIVE: 5,
    ONE: 1,
    QUARTER: 0.25,
    DIME: 0.1,
    NICKEL: 0.05,
    PENNY: 0.01,
}

function findNoteInCid(cid, denomination) {
    return cid.find((value) => value[0] == denomination)[1]
}

//main function
function checkCashRegister(price, cash, cid) {
    let change = cash - price;

    if (!change) {
        return
    }

    console.log(change)

    let changeBreakDown = breakChange(change, cid);
    let changeObjectFormat = getChangeObjectFormat(changeBreakDown, change, cid)

    console.log(changeObjectFormat)
    return changeObjectFormat
}


function breakChange(change, cid) {
    let newChange = change;
    let numberOfCurrencyUnitRequired = {}
    let exactAmountRequired = {}
    for (let key in currencyUnit) {
        if (currencyUnit[key] > change /*|| !findNoteInCid(cid, key)*/) {
        } else {
            // console.log(key, ': ', currencyUnit[key]);

            if (parseInt(newChange / currencyUnit[key])) {
                numberOfCurrencyUnitRequired[key] = parseInt(newChange / currencyUnit[key])
                if (numberOfCurrencyUnitRequired[key] * currencyUnit[key] > findNoteInCid(cid, key)) {
                    numberOfCurrencyUnitRequired[key] = findNoteInCid(cid, key) / currencyUnit[key];

                    newChange = parseFloat(((newChange % currencyUnit[key]) + ((parseInt(newChange / currencyUnit[key]) * currencyUnit[key]) - findNoteInCid(cid, key))).toFixed(2));

                } else {
                    newChange = parseFloat((newChange % currencyUnit[key]).toFixed(2))
                }
                exactAmountRequired[key] = numberOfCurrencyUnitRequired[key] * currencyUnit[key]
            }
        }
    }
    // console.log(numberOfCurrencyUnitRequired)
    // console.log(exactAmountRequired)

    return exactAmountRequired;

}

function getChangeObjectFormat(changeBreakDown, change, cid) {
    change = parseFloat(change.toFixed(2))
    let totalAvailableChange = 0;
    let totalMoneyInDrawer = 0;
    for (const key in changeBreakDown) {
        totalAvailableChange += changeBreakDown[key]
    }
    totalAvailableChange = parseFloat(totalAvailableChange.toFixed(2))
    // console.log(totalAvailableChange)

    cid.forEach((value) => {
        totalMoneyInDrawer += value[1]
    })
    totalMoneyInDrawer = parseFloat(totalMoneyInDrawer.toFixed(2))
    // console.log(totalMoneyInDrawer)

    let changeArray = []
    for (const key in changeBreakDown) {
        changeArray.push([key, changeBreakDown[key]])
    }


    if (totalAvailableChange == change && totalAvailableChange == totalMoneyInDrawer) {
        return { status: "CLOSED", change: changeArray }
    } else if (totalAvailableChange == change) {
        return { status: "OPEN", change: changeArray }
    } else if (totalAvailableChange < change) {
        return { status: "INSUFFICIENT_FUNDS", change: [] }
    }

}

checkCashRegister(3.26, 100, [
    ["PENNY", 1.01],
    ["NICKEL", 2.05],
    ["DIME", 3.1],
    ["QUARTER", 4.25],
    ["ONE", 90],
    ["FIVE", 55],
    ["TEN", 20],
    ["TWENTY", 60],
    ["ONE HUNDRED", 100]
])

checkCashRegister(3.26, 100, [["PENNY", 0.01], ["NICKEL", 0], ["DIME", 0], ["QUARTER", 0], ["ONE", 0], ["FIVE", 0], ["TEN", 0], ["TWENTY", 0], ["ONE HUNDRED", 0]])

checkCashRegister(19.5, 20, [["PENNY", 0.5], ["NICKEL", 0], ["DIME", 0], ["QUARTER", 0], ["ONE", 0], ["FIVE", 0], ["TEN", 0], ["TWENTY", 0], ["ONE HUNDRED", 0]])

